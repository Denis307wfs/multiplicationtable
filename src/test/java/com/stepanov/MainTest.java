package com.stepanov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Properties;

class MainTest {

    @Test
    void correctMainTest() {
        Assertions.assertDoesNotThrow(() -> Main.main(null));
    }

    @Test
    void loadInternalPropertyTest() {
        PropertyLoader loader = new PropertyLoader();
        Properties properties = loader.getProperty();
        Assertions.assertEquals("10", properties.getProperty("min"), "min is 10");
        Assertions.assertEquals("10", properties.getProperty("max"), "max is 10");
        Assertions.assertEquals("1", properties.getProperty("increment"), "increment is 1");
    }
}