package com.stepanov.multiplytable;

import com.stepanov.exception.TableException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

class FloatTableTest {
    private Properties properties;
    private FloatTable table;

    @BeforeEach
    void init() {
        properties = new Properties();
        properties.setProperty("min", "2.0f");
        properties.setProperty("max", "9.0f");
        properties.setProperty("increment", "3.0f");
        table = new FloatTable(properties);
    }

    @Test
    void trueValidTest() {
        Assertions.assertTrue(table.validateData());
    }

    @Test
    void throwExceptionValidTest() {
        properties.setProperty("increment", "-3.0f");
        Assertions.assertThrows(TableException.class, () -> new FloatTable(properties));
        properties.setProperty("increment", "0.0f");
        Assertions.assertThrows(TableException.class, () -> new FloatTable(properties));
    }

    @Test
    void revertMinMaxTest() {
        properties.setProperty("min", "20.0f");
        properties.setProperty("max", "2.0f");
        properties.setProperty("increment", "-3.0f");
        table = new FloatTable(properties);
        Assertions.assertTrue(table.validateData());
    }

    @Test
    void showRes1() {
        properties.setProperty("min", "3.0f");
        properties.setProperty("max", "3.0f");
        properties.setProperty("increment", "1.0f");
        table = new FloatTable(properties);
        Assertions.assertEquals("3.00 * 3.00 = 9.000", table.show());
    }

    @Test
    void showRes2() {
        properties.setProperty("min", "6.03f");
        properties.setProperty("max", "6.03f");
        table = new FloatTable(properties);
        Assertions.assertEquals("6.03 * 6.03 = 36.361", table.show());
    }

    @Test
    void showRes3() {
        properties.setProperty("min", "-1.01f");
        properties.setProperty("max", "-2.04f");
        properties.setProperty("increment", "-0.61f");
        table = new FloatTable(properties);
        String expected = "-2.04 * -2.04 = 4.162\n" + "-2.04 * -1.43 = 2.917\n" + "\n"
                + "-1.43 * -2.04 = 2.917\n" + "-1.43 * -1.43 = 2.045";
        Assertions.assertEquals(expected, table.show());
    }

    @Test
    void showRes4() {
        properties.setProperty("min", "2.01f");
        properties.setProperty("max", "2.02f");
        properties.setProperty("increment", "0.008");
        table = new FloatTable(properties);
        String expected = "2.01 * 2.01 = 4.040\n" + "2.01 * 2.02 = 4.056\n" + "\n" +
                "2.02 * 2.01 = 4.056\n" + "2.02 * 2.02 = 4.072";
        Assertions.assertEquals(expected, table.show());
    }

}