package com.stepanov.multiplytable;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

class TableFactoryTest {
    private Properties properties;
    private Table<?> table;

    @BeforeEach
    void init() {
        properties = new Properties();
        properties.setProperty("min", "2");
        properties.setProperty("max", "9");
        properties.setProperty("increment", "3");

    }

    @Test
    void floatClassTest() {
        System.setProperty("type", "float");
        String type = System.getProperty("type");
        table = TableFactory.getTable(properties, type);
        Class<? extends Table> tableClass = table.getClass();
        Assertions.assertEquals(FloatTable.class, tableClass);
    }

    @Test
    void doubleClassTest() {
        System.setProperty("type", "double");
        String type = System.getProperty("type");
        table = TableFactory.getTable(properties, type);
        Class<? extends Table> tableClass = table.getClass();
        Assertions.assertEquals(DoubleTable.class, tableClass);
    }

    @Test
    void longClassTest() {
        System.setProperty("type", "long");
        String type = System.getProperty("type");
        table = TableFactory.getTable(properties, type);
        Class<? extends Table> tableClass = table.getClass();
        Assertions.assertEquals(LongTable.class, tableClass);
    }

    @Test
    void integerClassTest() {
        System.setProperty("type", "");
        String type = System.getProperty("type");
        table = TableFactory.getTable(properties, type);
        Class<? extends Table> tableClass = table.getClass();
        Assertions.assertEquals(IntegerTable.class, tableClass);
    }
}
