package com.stepanov.multiplytable;

import com.stepanov.exception.TableException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;


class IntegerTableTest {
    private Properties properties;
    private IntegerTable table;

    @BeforeEach
    void init() {
        properties = new Properties();
        properties.setProperty("min", "2");
        properties.setProperty("max", "9");
        properties.setProperty("increment", "3");
        table = new IntegerTable(properties);
    }

    @Test
    void trueValidTest() {
        Assertions.assertTrue(table.validateData());
    }

    @Test
    void throwExceptionValidTest() {
        properties.setProperty("increment", "-3");
        Assertions.assertThrows(TableException.class, () -> new IntegerTable(properties));
        properties.setProperty("increment", "0");
        Assertions.assertThrows(TableException.class, () -> new IntegerTable(properties));
    }

    @Test
    void revertMinMaxTest() {
        properties.setProperty("min", "20");
        properties.setProperty("max", "2");
        properties.setProperty("increment", "-3");
        table = new IntegerTable(properties);
        Assertions.assertTrue(table.validateData());
    }

    @Test
    void showRes1() {
        properties.setProperty("min", "3");
        properties.setProperty("max", "3");
        properties.setProperty("increment", "1");
        table = new IntegerTable(properties);
        Assertions.assertEquals("3 * 3 = 9", table.show());
    }

    @Test
    void showRes2() {
        properties.setProperty("min", "6");
        properties.setProperty("max", "3");
        properties.setProperty("increment", "-2");
        table = new IntegerTable(properties);
        String expected = "3 * 3 = 9\n" + "3 * 5 = 15\n" + "\n" + "5 * 3 = 15\n" + "5 * 5 = 25";
        Assertions.assertEquals(expected, table.show());
    }

    @Test
    void showRes3() {
        properties.setProperty("min", "0");
        properties.setProperty("max", "-3");
        properties.setProperty("increment", "-2");
        table = new IntegerTable(properties);
        String expected = "-3 * -3 = 9\n" + "-3 * -1 = 3\n" + "\n" + "-1 * -3 = 3\n" + "-1 * -1 = 1";
        Assertions.assertEquals(expected, table.show());
    }
}