package com.stepanov.multiplytable;

import com.stepanov.exception.TableException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

class DoubleTableTest {
    private Properties properties;
    private DoubleTable table;

    @BeforeEach
    void init() {
        properties = new Properties();
        properties.setProperty("min", "2.0");
        properties.setProperty("max", "9.0");
        properties.setProperty("increment", "3.0");
        table = new DoubleTable(properties);
    }

    @Test
    void trueValidTest() {
        Assertions.assertTrue(table.validateData());
    }

    @Test
    void throwExceptionValidTest() {
        properties.setProperty("increment", "-3.0");
        Assertions.assertThrows(TableException.class, () -> new DoubleTable(properties));
        properties.setProperty("increment", "0.0");
        Assertions.assertThrows(TableException.class, () -> new DoubleTable(properties));
    }

    @Test
    void revertMinMaxTest() {
        properties.setProperty("min", "20.0");
        properties.setProperty("max", "2.0");
        properties.setProperty("increment", "-3.0");
        table = new DoubleTable(properties);
        Assertions.assertTrue(table.validateData());
    }

    @Test
    void showRes1() {
        properties.setProperty("min", "3.0");
        properties.setProperty("max", "3.0");
        properties.setProperty("increment", "1.0");
        table = new DoubleTable(properties);
        Assertions.assertEquals("3.00 * 3.00 = 9.000", table.show());
    }

    @Test
    void showRes2() {
        properties.setProperty("min", "6.03");
        properties.setProperty("max", "6.03");
        table = new DoubleTable(properties);
        Assertions.assertEquals("6.03 * 6.03 = 36.361", table.show());
    }

    @Test
    void showRes3() {
        properties.setProperty("min", "5.01");
        properties.setProperty("max", "4.04");
        properties.setProperty("increment", "-0.61");
        table = new DoubleTable(properties);
        String expected = "4.04 * 4.04 = 16.322\n" + "4.04 * 4.65 = 18.786\n" + "\n" +
                "4.65 * 4.04 = 18.786\n" + "4.65 * 4.65 = 21.623";
        Assertions.assertEquals(expected, table.show());
    }

    @Test
    void showRes4() {
        properties.setProperty("min", "2.01");
        properties.setProperty("max", "2.02");
        properties.setProperty("increment", "0.008");
        table = new DoubleTable(properties);
        String expected = "2.01 * 2.01 = 4.040\n" + "2.01 * 2.02 = 4.056\n" + "\n" +
                "2.02 * 2.01 = 4.056\n" + "2.02 * 2.02 = 4.072";
        Assertions.assertEquals(expected, table.show());
    }

}