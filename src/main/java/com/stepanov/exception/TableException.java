package com.stepanov.exception;

public class TableException extends RuntimeException {

    public TableException(String message) {
        super(message);
    }
}
