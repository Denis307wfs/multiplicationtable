package com.stepanov;

import com.stepanov.multiplytable.Table;
import com.stepanov.multiplytable.TableFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);
    private static final Logger logger = LoggerFactory.getLogger("LOGGER");

    public static void main(String[] args) {
        PropertyLoader loader = new PropertyLoader();
        Properties properties = loader.getProperty();

        String type = System.getProperty("type");
        LOG.debug("Chosen type - {}", type);

        Table<?> table = TableFactory.getTable(properties, type);
        String showTable = table.show();
        logger.info(showTable);
    }
}
