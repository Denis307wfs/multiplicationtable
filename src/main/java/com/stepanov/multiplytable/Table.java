package com.stepanov.multiplytable;

import com.stepanov.exception.TableException;

import java.util.Properties;

public abstract class Table<T extends Number> {
    protected T min;
    protected T max;
    protected T increment;

    protected Table(Properties properties) {
        init(properties);
        validateData();
    }

    protected abstract void init(Properties properties);

    protected abstract boolean validateData() throws TableException;

    public abstract String show();

}
