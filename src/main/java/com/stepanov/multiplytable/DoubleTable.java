package com.stepanov.multiplytable;

import com.stepanov.exception.TableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Properties;

public class DoubleTable extends Table<Double> {
    private static final Logger log = LoggerFactory.getLogger(DoubleTable.class);

    public DoubleTable(Properties properties) {
        super(properties);
    }

    @Override
    protected void init(Properties properties) {
        min = Double.parseDouble(properties.getProperty("min"));
        max = Double.parseDouble(properties.getProperty("max"));
        increment = Double.parseDouble(properties.getProperty("increment"));
    }

    @Override
    protected boolean validateData() {
        if ((min > max && increment > 0)
                || (min < max && increment < 0) ||
                increment == 0) {
            String message = String.format("Incorrect data (min: %f, max: %f, increment: %f)", min, max, increment);
            log.error(message);
            throw new TableException(String.format("Incorrect data (min: %f, max: %f, increment: %f)", min, max, increment));
        }

        if (min > max) {
            log.info("Min more then max and increment < 0. So max = min and min = max");
            double temp = min;
            min = max;
            max = temp;
            increment *= -1;
        }
        return true;
    }

    @Override
    public String show() {
        StringBuilder sb = new StringBuilder();
        Locale.setDefault(Locale.US);
        for (double i = min; i <= max; i = increment(i)) {
            for (double j = min; j <= max; j = increment(j)) {
                sb.append(String.format("%.2f * %.2f = %.3f", i, j, i * j));
                sb.append("\n");
            }
            sb.append("\n");
        }
        sb.delete(sb.length()-2, sb.length());
        return sb.toString();
    }

    private double increment(double before) {
        return before + increment;
    }
}
