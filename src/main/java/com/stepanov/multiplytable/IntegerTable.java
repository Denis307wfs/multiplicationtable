package com.stepanov.multiplytable;

import com.stepanov.exception.TableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;


public class IntegerTable extends Table<Integer> {
    private static final Logger log = LoggerFactory.getLogger(IntegerTable.class);

    public IntegerTable(Properties properties) {
        super(properties);
    }

    @Override
    protected void init(Properties properties) {
        min = Integer.parseInt(properties.getProperty("min"));
        max = Integer.parseInt(properties.getProperty("max"));
        increment = Integer.parseInt(properties.getProperty("increment"));
    }

    @Override
    protected boolean validateData() {
        if ((min > max && increment > 0)
        || (min < max && increment < 0) ||
        increment == 0) {
            String message = String.format("Incorrect data (min: %d, max: %d, increment: %d)", min, max, increment);
            log.error(message);
            throw new TableException(String.format("Incorrect data (min: %d, max: %d, increment: %d)", min, max, increment));
        }

        if (min > max) {
            log.info("Min more then max and increment < 0. So max = min and min = max");
            int temp = min;
            min = max;
            max = temp;
            increment *= -1;
        }
        return true;
    }

    @Override
    public String show() {
        StringBuilder sb = new StringBuilder();
        for (int i = min; i <= max; i = increment(i)) {
            for (int j = min; j <= max; j = increment(j)) {
                sb.append(String.format("%d * %d = %d", i, j, i * j));
                sb.append("\n");
            }
            sb.append("\n");
        }
        sb.delete(sb.length()-2, sb.length());
        return sb.toString();
    }

    private int increment(int before) {
        return before + increment;
    }
}
