package com.stepanov.multiplytable;

import java.util.Properties;

public class TableFactory {

    private TableFactory() {
    }

    public static Table<?> getTable(Properties properties, String type) {
        Table<?> table;
        if (type == null || type.isEmpty()) {
            type = "INTEGER";
        }
        switch (type.toUpperCase()) {
            case "DOUBLE":
                table = new DoubleTable(properties);
                break;
            case "LONG":
                table = new LongTable(properties);
                break;
            case "FLOAT":
                table = new FloatTable(properties);
                break;
            default:
                table = new IntegerTable(properties);
        }
        return table;
    }
}
