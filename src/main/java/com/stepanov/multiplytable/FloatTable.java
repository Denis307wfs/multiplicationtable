package com.stepanov.multiplytable;

import com.stepanov.exception.TableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Properties;

public class FloatTable extends Table<Float> {
    private static final Logger log = LoggerFactory.getLogger(FloatTable.class);

    public FloatTable(Properties properties) {
        super(properties);
    }

    @Override
    protected void init(Properties properties) {
        min = Float.parseFloat(properties.getProperty("min"));
        max = Float.parseFloat(properties.getProperty("max"));
        increment = Float.parseFloat(properties.getProperty("increment"));
    }

    @Override
    protected boolean validateData() {
        if ((min > max && increment > 0)
                || (min < max && increment < 0) ||
                increment == 0) {
            String message = String.format("Incorrect data (min: %f, max: %f, increment: %f)", min, max, increment);
            log.error(message);
            throw new TableException(String.format("Incorrect data (min: %f, max: %f, increment: %f)", min, max, increment));
        }

        if (min > max) {
            log.info("Min more then max and increment < 0. So max = min and min = max");
            float temp = min;
            min = max;
            max = temp;
            increment *= -1;
        }
        return true;
    }

    @Override
    public String show() {
        StringBuilder sb = new StringBuilder();
        Locale.setDefault(Locale.US);
        for (float i = min; i <= max; i = increment(i)) {
            for (float j = min; j <= max; j = increment(j)) {
                sb.append(String.format("%.2f * %.2f = %.3f", i, j, i * j));
                sb.append("\n");
            }
            sb.append("\n");
        }
        sb.delete(sb.length()-2, sb.length());
        return sb.toString();
    }

    private float increment(float before) {
        return before + increment;
    }

}
